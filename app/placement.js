const express = require('express');

const createRouter = connection => {
    const router = express.Router();

    router.get('/', (req, res) => {
        connection.query('SELECT * FROM `placeItem`', (error, results) => {
            if (error) {
                res.status(500).send({error: 'Database error'});
            }
            res.send(results);
        })
    });

    router.get('/:id', (req, res) => {
        connection.query('SELECT * FROM `placeItem` WHERE id = ?', req.params.id, (error, results) => {
            if (error) {
                res.status(500).send({error: 'Database error'});
            }

            if (results[0]) {
                res.send(results[0])
            } else {
                res.status(404).send({error: 'Item not found'});
            }
        })
    });

    router.post('/', (req, res) => {
        const placement = req.body;

        connection.query('INSERT INTO `placeItem` (`title`, `description`) VALUES (?, ?)',
            [placement.title, placement.description],
            (error, results) => {
                if (error) {
                    res.status(500).send({error: 'Database error'});
                }
                res.send({message: 'OK'});
            });
    });

    router.delete('/:id', (req, res) => {
        connection.query('DELETE FROM `placeItem` WHERE `id` = ?', req.params.id, (error, results) => {
            if (error) {
                res.status(500).send({error: 'Database error'});
            }
            res.send({message: 'Delete ok'});
        });
    });

    return router;
};

module.exports = createRouter;