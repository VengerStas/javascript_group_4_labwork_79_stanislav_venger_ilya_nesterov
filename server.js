const express = require('express');
const cors = require('cors');
const mysql = require('mysql');

const item = require('./app/item');
const placement = require('./app/placement');
const category = require('./app/category');


const app = express();
app.use(cors());
app.use(express.json());

const port = 8000;


const connection = mysql.createConnection( {
    host: "localhost",
    user: "stan",
    password: "qwerty123",
    database: "inventory"
});

app.use('/item', item(connection));
app.use('/placement', placement(connection));
app.use('/category', category(connection));



connection.connect(err => {
    if (err) {
        console.error("error" + err.stack);
        return;
    }
    console.log("Connection to sql" + connection.threadId);

    app.listen(port, () => {
        console.log("Our port " + port)
    })
});