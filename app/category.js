const express = require('express');

const createRouter = connection => {
    const router = express.Router();

    router.get('/', (req, res) => {
        connection.query('SELECT * FROM `categoryItem`', (error, results) => {
            if (error) {
                res.status(500).send({error: 'Database error'});
            }
            res.send(results);
        })
    });

    router.get('/:id', (req, res) => {
        connection.query('SELECT * FROM `categoryItem` WHERE id = ?', req.params.id, (error, results) => {
            if (error) {
                res.status(500).send({error: 'Database error'});
            }

            if (results[0]) {
                res.send(results[0])
            } else {
                res.status(404).send({error: 'Item not found'});
            }
        })
    });

    router.post('/', (req, res) => {
        const category = req.body;

        connection.query('INSERT INTO `categoryItem` (`title`, `description`) VALUES (?, ?)',
            [category.title, category.description],
            (error, results) => {
                if (error) {
                    res.status(500).send({error: 'Database error'});
                }
                res.send({message: 'OK'});
            });
    });

    router.delete('/:id', (req, res) => {
        connection.query('DELETE FROM `categoryItem` WHERE `id` = ?', req.params.id, (error, results) => {
            if (error) {
                res.status(500).send({error: 'Database error'});
            }
            res.send({message: 'Delete ok'});
        });
    });

    return router;
};

module.exports = createRouter;