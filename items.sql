CREATE SCHEMA `inventory` DEFAULT CHARACTER SET utf8 ;

USE `inventory`;

CREATE TABLE `categoryItem` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `description` TEXT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `placeItem` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(255) NOT NULL,
  `description` TEXT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `item` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `categoryId` INT NULL,
  `placesId` INT NULL,
  `title` VARCHAR(255) NOT NULL,
  `description` TEXT NULL,
  `dateOfRegistration` VARCHAR(255) NOT NULL,
  `image` VARCHAR(100) NULL,
  PRIMARY KEY (`id`),
  INDEX `category_id_fk_idx` (`categoryId` ASC),
  INDEX `places_id_fk_idx` (`placesId` ASC),
  CONSTRAINT `category_id_fk`
    FOREIGN KEY (`categoryId`)
    REFERENCES `inventory`.`categoryItem` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `places_id_fk`
    FOREIGN KEY (`placesId`)
    REFERENCES `inventory`.`placeItem` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE);
SELECT `item` .*, `categoryItem`.`title` AS `category_title` FROM `item`
LEFT JOIN `categoryItem` ON `item`.`categoryId` = `categoryItem`.`id`;
SELECT `item` .*, `placeItem`.`title` AS `place_title` FROM `item`
LEFT JOIN `placeItem` ON `item`.`placesId` = `placeItem`.`id`;     

