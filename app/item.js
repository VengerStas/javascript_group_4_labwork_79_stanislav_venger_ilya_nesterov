const express = require('express');

const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const createRouter = connection => {
    const router = express.Router();

    router.get('/', (req, res) => {
        connection.query('SELECT * FROM `item`', (error, results) => {
            if (error) {
                res.status(500).send({error: 'Database error'});
            }
            res.send(results);
        })
    });

    router.get('/:id', (req, res) => {
        connection.query('SELECT * FROM `item` WHERE `id` = ?', req.params.id, (error, results) => {
            if (error) {
                res.status(500).send({error: 'Database error'});
            }

            if (results[0]) {
                res.send(results[0])
            } else {
                res.status(404).send({error: 'Item not found'});
            }
        })
    });

    router.post('/', upload.single('image'), (req, res) => {
        const item = req.body;

        if (req.file) {
            item.image = req.file.filename;
        }

        connection.query('INSERT INTO `item` (`categoryId`, `placesId`, `title`, `description`, `dateOfRegistration`, `image`) VALUES (?, ?, ?, ?, ?, ?)',
            [item.categoryId, item.placesId, item.title, item.description, item.dateOfRegistration, item.image],
            (error, results) => {
                if (error) {
                    res.status(500).send({error: 'Database error'});
                }
                res.send({message: 'OK'});
            });
    });

    router.put('/:id', (req, res) => {
        const item = req.body;
        connection.query('UPDATE `item` SET `title`="' + item.title + '", `description`="' + item.description + '", `dateOfRegistration`="'+ item.dateOfRegistration +'" WHERE id = ?', req.params.id, (error, results) => {
            if (error) {
                res.status(500).send({error: 'Database error'});
            }
            res.send({message: 'OK'});
        });
    });

    router.delete('/:id', (req, res) => {
       connection.query('DELETE FROM `item` WHERE `id` = ?', req.params.id, (error, results) => {
                if (error) {
                    res.status(500).send({error: 'Database error'});
                }
                res.send({message: 'Delete ok'});
           });

    });

    return router;
};

module.exports = createRouter;